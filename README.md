Axiomtek Documentation
======================

This repository contains documentations for Axiomtek's boards

SCM180-180-EVK
--------------
* [Linux User's Manual](https://gitlab.com/axiomtek/manual/-/wikis/SCM180-Linux-User's-Manual)
* [Programmer Guide](https://gitlab.com/axiomtek/manual/-/wikis/SCM180-Programmer-Guide)

ICO330
------
* [Linux User's Manual](https://gitlab.com/axiomtek/manual/-/wikis/ICO330-Linux-User's-Manual)
